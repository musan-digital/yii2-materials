<?php

namespace lafacoder\modules\materials\models;

use lafacoder\modules\materials\behaviors\BlocksBehavior;
use lafacoder\modules\materials\Module;
use mitrii\attachments\behaviors\AttributesAttachmentBehavior;
use yii\db\ActiveRecord;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "material".
 *
 * @property integer $id
 * @property string $title
 * @property string $subtitle
 * @property string $slug
 * @property string $publish_date
 * @property integer $views
 * @property integer $is_active
 * @property integer $is_delete
 * @property string $create_date
 * @property string $update_date
 * @property string $type
 * @property string $image
 *
 */
class Material extends ActiveRecord
{
    public $material_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'materials';
    }

    public static function types()
    {
        return array_merge([
            'materials' => [
                'class' => Material::className(),
                'label' => 'Материалы',
                'blockTypes' => ['Heading', 'Text', 'Quote', 'Image', 'Video']
            ],
        ], Yii::$app->getModule('materials')->getInstance()->types);

    }

    public static function getType($type)
    {
        $types = Material::types();

        if (isset($types[$type]))
        {
            return $types[$type];
        }
    }

    public static function find()
    {
        return new MaterialQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [
            [['type'], 'required'],

            [['title'], 'required'],

            [['slug'], 'unique'],
            [
                [
                    'user_id',
                    'views',
                    'is_active',
                    'is_delete'
                ],
                'integer'
            ],
            [['subtitle', 'image', 'publish_date', 'type', 'raw_blocks_ru'], 'string'],
            [['title', 'slug'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'title' => 'Заголовок',
            'subtitle' => 'Подзаголовок',
            'slug' => 'ЧПУ',
            'publish_date' => 'Дата публикаций',
            'views' => 'Кол-во просмотров',
            'is_active' => 'Активен',
            'type' => 'Тип',
            'credit' => 'Автор цитаты',
            'create_date' => 'Дата создания',
            'update_date' => 'Дата изменений',
            'is_featured' => 'Закрепить',
            'artist_id' => 'Исполнитель'
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date', 'update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date']
                ],
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
                'immutable' => true,
                'ensureUnique' => true,
            ],
            [
                'class' => BlocksBehavior::className(),
            ],
            [
                'class' => AttributesAttachmentBehavior::className(),
                'attributes' => [
                    'image'
                ],
            ],
            [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_delete' => true
                ],
                'replaceRegularDelete' => true
            ],

        ];
    }

    public function getAllBlocks()
    {
        return $this->hasMany(Block::className(), ['material_id' => 'id']);
    }


    public function getBlocks()
    {
        return $this->getBlocksForLang(Yii::$app->language);
    }

    public function getBlocksForLang($lang)
    {
        return $this->hasMany(Block::className(), ['material_id' => 'id'])
            ->andOnCondition(['lang' => $lang]);
    }

    public static function instantiate($row)
    {
        $types = Material::types();

        if (isset($types[$row['type']]))
        {
            return new $types[$row['type']]['class'];
        }
    }

}
