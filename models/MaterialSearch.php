<?php

namespace lafacoder\modules\materials\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use lafacoder\modules\materials\models\Material;

/**
 * MaterialSearch represents the model behind the search form about `common\modules\materials\models\Material`.
 */
class MaterialSearch extends Material
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'type', 'is_active', 'is_delete', 'views'], 'integer'],
            [['title', 'subtitle', 'image', 'create_date', 'update_date', 'publish_date', 'slug'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @property integer $type
     * @return ActiveDataProvider
     */
    public function search($params, $type = null)
    {
        $query = Material::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'is_delete' => false,
        ]);
        
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'publish_date' => $this->publish_date,
            'is_active' => $this->is_active,
            'is_delete' => $this->is_delete,
            'views' => $this->views,
            'type' => $type,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        $query->orderBy('publish_date DESC');
        
        return $dataProvider;
    }
}
