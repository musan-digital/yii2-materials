<?php
/**
 * Created by PhpStorm.
 * User: yevgeniy
 * Date: 11/4/14
 * Time: 5:05 PM
 */

namespace lafacoder\modules\materials\models;


use yii\db\ActiveQuery;
use yii\db\Expression;

class MaterialQuery extends ActiveQuery
{
    public $type;

    public function prepare($builder)
    {
        if ($this->type !== null) {
            $this->andWhere(['type' => $this->type]);
        }
        return parent::prepare($builder);
    }

    public function published()
    {
        $this->andWhere(['is_active' => 1, 'is_delete' => 0])->andWhere(['<=', 'publish_date', new Expression('NOW()')]);

        return $this;
    }
    
    public function orderDate()
    {
        $this->addOrderBy(['publish_date' => SORT_DESC]);

        return $this;
    }
    
}
