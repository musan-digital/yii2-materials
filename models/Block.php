<?php

namespace lafacoder\modules\materials\models;

use mitrii\attachments\behaviors\AttributesAttachmentBehavior;
use Yii;

/**
 * This is the model class for table "blocks".
 *
 * @property integer $id
 * @property integer $material_id
 * @property string $value
 * @property string $code
 * @property integer $type
 * @property integer $order
 * @property integer $lang
 * @property integer $is_deleted
 */
class Block extends \yii\db\ActiveRecord
{

    const TYPE_HEADING 		= 'heading';
    const TYPE_TEXT 		= 'text';
    const TYPE_VIDEO 		= 'video';
    const TYPE_AUDIO 		= 'audio';
	const TYPE_SONG 		= 'song';
	const TYPE_IMAGE 		= 'image';
	const TYPE_QUOTE 		= 'quote';
	const TYPE_LYRICS 		= 'lyrics';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['material_id', 'order', 'is_deleted'], 'integer'],
            [['value', 'code', 'lang', 'type'], 'string']
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributesAttachmentBehavior::className(),
                'attributes' => [
                    'value',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'material_id' => 'Material ID',
            'value' => 'Value',
            'code' => 'Code',
            'type' => 'Block Type ID',
            'order' => 'Order',
            'lang' => 'Lang',
            'is_deleted' => 'Is Deleted',
        ];
    }

    public function getMaterial()
    {
      return $this->hasOne(Material::className(), ['id' => 'material_id']);
    }
}
