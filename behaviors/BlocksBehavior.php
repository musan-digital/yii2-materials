<?php
/**
 * Poject: gakku2
 * User: mitrii
 * Date: 3.10.2016
 * Time: 15:04
 * Original File Name: BlocksBehavior.php
 */

namespace lafacoder\modules\materials\behaviors;


use lafacoder\modules\materials\models\Block;
use mitrii\attachments\helpers\Audio;
use mitrii\attachments\helpers\Image;
use mitrii\attachments\models\Attachment;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;

class BlocksBehavior extends Behavior
{
    private $raw_blocks = ['ru' => '', 'kk' =>''];

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_UPDATE => 'saveBlocks',
            ActiveRecord::EVENT_AFTER_INSERT => 'saveBlocks',
        ];
    }

    /**
     * @inheritdoc
     */
    public function canGetProperty($name, $checkVars = true)
    {
        return (in_array($name, ['raw_blocks_ru', 'raw_blocks_kk'])) ? true : parent::canGetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function canSetProperty($name, $checkVars = true)
    {
        return (in_array($name, ['raw_blocks_ru', 'raw_blocks_kk'])) ? true : parent::canSetProperty($name, $checkVars);
    }

    protected function getRawBlocks($lang = 'ru')
    {
        if (!empty($this->raw_blocks[$lang])) return $this->raw_blocks[$lang];

        $blocks = Block::find()->where(['material_id' => $this->owner->id, 'lang' => $lang])->all();
        if($blocks == !null)
        {
            $blocks_array = [];
            foreach($blocks as $block)
            {
                $blocks_array[] = $this->blockToJson($block);
            }
            $this->raw_blocks[$lang] = Json::encode([
                'data' => $blocks_array
            ]);
        }
        return $this->raw_blocks[$lang];
    }

    public function getRaw_blocks_ru()
    {
        return $this->getRawBlocks('ru');
    }

    public function getRaw_blocks_kk()
    {
        return $this->getRawBlocks('kk');
    }

    public function setRaw_Blocks_ru($value)
    {
        $this->raw_blocks['ru']= $value;
    }

    public function setRaw_Blocks_kk($value)
    {
        $this->raw_blocks['kk']= $value;
    }

    public function saveBlocks()
    {

        foreach ($this->raw_blocks as $lang => $raw_blocks)
        {
            if(!empty($raw_blocks)){
                Block::deleteAll(['material_id' => $this->owner->id, 'lang' => $lang]);

                $raw_blocks = Json::decode($raw_blocks);

                $i = 0;
                foreach($raw_blocks['data'] as $raw_block) {
                    $i++;
                    $block = $this->jsonToBlock($raw_block);
                    $block->material_id = $this->owner->id;
                    $block->order = $i;
                    $block->lang = $lang;
                    $block->save();
                }
            }
        }
    }

    public function jsonToBlock($item)
    {
        $block = new Block();
        switch ($item['type'])
        {
            case 'text':
            case 'heading':
                $block->type = $item['type'];
                $block->value = $item['data']['text'];
                break;
            case 'image':
                $block->type = $item['type'];
                $block->value = $item['data']['hash'];
                break;
            case 'audio':
                $block->type = $item['type'];
                $block->value = $item['data']['hash'];
                break;
            case 'video':
                $block->type = $item['type'];
                $block->value = $item['data']['remote_id'];
                break;
            case 'quote':
                $block->type = $item['type'];
                $block->value = Json::encode([
                    'text' => $item['data']['text'],
                    'format' => $item['data']['format'],
                    'cite' => $item['data']['cite'],
                ]);
                break;
            case 'song':
                $block->type = $item['type'];
                $block->value = $item['data']['remote_id'];
                break;
            case 'lyrics':
                $block->type = $item['type'];
                $block->value = Json::encode([
                    'text' => $item['data']['text'],
                    'caption' => $item['data']['caption'],
                    'words' => $item['data']['words'],
                    'music' => $item['data']['music']
                ]);
                break;
            case 'gallery':
                $block->type = $item['type'];
                $hash = array_column($item['data'], 'hash');
                $block->value = Json::encode($hash);
                break;
            default:
                break;
        }
        return $block;
    }

    public function blockToJson($block)
    {
        $item = [];
        switch ($block->type)
        {
            case 'heading':
                $item = [
                    'type' => $block->type,
                    'data' => [
                        'text' => $block->value
                    ]
                ];
                break;
            case 'text':
                $item = [
                    'type' => $block->type,
                    'data' => [
                        'text' => $block->value,
                        'format' => 'html',
                    ]
                ];
                break;
            case 'image':
                $item = [
                    'type' => $block->type,
                    'data' => [
                        'hash' => $block->value,
                        'filelink' => Image::url($block->value, 640, 480),
                        'file' => [
                            'url' => Image::url($block->value, 640, 480),
                        ],
                    ],
                ];
                break;
            case 'gallery':
                $data = [];
                foreach (Json::decode($block->value) as $hash) {
                    $data[] = [
                        'hash' => $hash,
                        'filelink' => Image::url($hash, 100, 100)
                    ];
                }

                $item = [
                    'type' => $block->type,
                    'data' => $data,
                ];
                break;
            case 'audio':
                $attachment = Attachment::findOne(['hash' => $block->value]);
                if(!empty($attachment)) {
                    $item = [
                        'type' => $block->type,
                        'data' => [
                            'hash' => $block->value,
                            'filelink' => Audio::url($block->value),
                            'file' => [
                                'url' => Audio::url($block->value),
                                'type' => $attachment->type,
                            ],
                        ],
                    ];
                }
                break;
            case 'video':
                $item = [
                    'type' => $block->type,
                    'data' => [
                        'source' => 'youtube',
                        'remote_id' => $block->value
                    ]
                ];
                break;
            case 'quote':
                $item = [
                    'type' => $block->type,
                    'data' => Json::decode($block->value),
                ];
                break;
            case 'song':
                $item = [
                    'type' => $block->type,
                    'data' => [
                        'remote_id' => $block->value
                    ]
                ];
                break;
            case 'lyrics':
                $item = [
                    'type' => $block->type,
                    'data' => Json::decode($block->value)
                ];
                break;

            default:
                break;
        }
        return $item;
    }

}