<?php
/**
 * Created by PhpStorm.
 * User: titanus
 * Date: 05.09.16
 * Time: 18:07
 */

namespace lafacoder\modules\materials\behaviors;


use mitrii\attachments\helpers\Image;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use understeam\httpclient\Client;

class YouTubeImageDownloadBehavior extends Behavior
{
    public $enable = false;
    public $attributes = [];

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_UPDATE => 'downloadImageYouTube',
            ActiveRecord::EVENT_AFTER_INSERT => 'downloadImageYouTube',
        ];
    }

    public function downloadImageYouTube()
    {
        if ($this->enable == true) {
            foreach ($this->attributes as $attribute) {
                $client = new Client();
                $response = $client->createRequest()
                    ->setMethod('get')
                    ->setUrl('https://www.googleapis.com/youtube/v3/videos?key=' . \Yii::$app->params['YOU_TUBE_API_KEY'] . '&part=snippet&id=' . $this->owner->video . '')
                    ->send();
                if (!empty($response->data['items'])) {
                    $image_array = $response->data['items'][0]['snippet']['thumbnails'];
                    $maxwidth = 0;
                    foreach ($image_array as $image) {
                        if ($image['width'] > $maxwidth) {
                            if (Image::path($this->owner->$attribute) == null) {
                                $this->owner->$attribute = $image['url'];
                            }
                            $maxwidth = $image['width'];
                        }
                    }
                }
            }
        }
    }
}