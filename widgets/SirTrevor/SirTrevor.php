<?php

namespace lafacoder\modules\materials\widgets\SirTrevor;

use lafacoder\modules\materials\widgets\SirTrevor\assets\SirTrevorAsset;
use lafacoder\modules\materials\widgets\SirTrevor\assets\WidgetAsset;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\widgets\InputWidget;

class SirTrevor extends InputWidget
{
    public $debug = YII_DEBUG;
    public $language = 'ru';
    public $iconUrl;

    /**
     * @var null
     */
    public $uploadUrl = '';

    public $blockTypes = ["Heading", "Text", "List", "Quote", "Image", "Video", "Tweet"];

    public function init()
    {
        $bundle = SirTrevorAsset::register($this->view);
        $this->iconUrl = (empty($this->iconUrl)) ? $bundle->baseUrl . '/build/sir-trevor-icons.svg' : $this->iconUrl;

        if (empty($this->options['id']) ) {
            $this->options['id'] = $this->hasModel() ? Html::getInputId($this->model, $this->attribute) : $this->id;
        }

        parent::init();
    }

    /**
     * Render the text area input
     */
    protected function renderInput()
    {
        if ($this->hasModel()) {
            $input = Html::activeTextArea($this->model, $this->attribute, $this->options);
        } else {
            $input = Html::textArea($this->name, $this->value, $this->options);
        }

        return $input;
    }

    public function run()
    {
        $editor_options = Json::encode([
            'el' => new JsExpression("document.querySelector('#{$this->options['id']}')"),
            'blockTypes' => $this->blockTypes,
            'defaultType' => false,
        ]);

        $debug = ($this->debug) ? "SirTrevor.config.debug = true;" : '';

        $this->view->registerJs("
             {$debug}
             SirTrevor.config.language = '{$this->language}';
             SirTrevor.setDefaults({
                uploadUrl: '{$this->uploadUrl}',
                iconUrl: '$this->iconUrl'
             });
                
        new SirTrevor.Editor({$editor_options});") ;

        return $this->renderInput();

    }
}