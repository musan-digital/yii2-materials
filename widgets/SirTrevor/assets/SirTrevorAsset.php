<?php
/**
 * Created by PhpStorm.
 * User: titanus
 * Date: 17.10.16
 * Time: 12:08
 */

namespace lafacoder\modules\materials\widgets\SirTrevor\assets;


class SirTrevorAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/sir-trevor-js';
    public $css = [
        'build/sir-trevor.css'
    ];
    public $js = [
        'build/sir-trevor.js',
        'locales/ru.js',
    ];
    public $depends = [
        'lafacoder\modules\materials\widgets\SirTrevor\assets\ES5ShimAsset',
    ];
}