<?php
/**
 * Created by PhpStorm.
 * User: titanus
 * Date: 17.10.16
 * Time: 12:01
 */

namespace lafacoder\modules\materials\widgets\SirTrevor\assets;


class WidgetAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@lafacoder/modules/materials/widgets/SirTrevor/assets';
    public $publishOptions = [
        'only' => [
            'css/*',
            'css/demo-files/*',
            'css/fonts/*',
            'js/*',
        ]
    ];
    public $css = [
        'css/style.css'
    ];
    public $js = [
        'js/ru.js',
    ];
    public $depends = [
        'lafacoder\modules\materials\widgets\SirTrevor\assets\SirTrevorAsset',
    ];
}