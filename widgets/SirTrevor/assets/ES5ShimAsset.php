<?php
/**
 * Poject: gakku2
 * User: mitrii
 * Date: 3.03.2016
 * Time: 17:46
 * Original File Name: ES5Shim.php
 */

namespace lafacoder\modules\materials\widgets\SirTrevor\assets;


use yii\web\AssetBundle;

class ES5ShimAsset extends AssetBundle
{
    public $sourcePath = '@bower/es5-shim';
    public $css = [
    ];
    public $js = [
        'es5-shim.min.js'
    ];
    public $depends = [
    ];
}