SirTrevor.Blocks.Quote = SirTrevor.Block.extend({

    type: "quote",

    title: function() { return i18n.t('blocks:quote:title'); },

    icon_name: 'quote',

    editorHTML: function() {
        var template = _.template([
            '<blockquote class="st-required st-text-block" contenteditable="true"></blockquote>',
            '<label class="st-input-label"> <%= i18n.t("blocks:quote:credit_field") %></label>',
            '<input maxlength="140" name="cite" placeholder="<%= i18n.t("blocks:quote:credit_field") %>"',
            ' class="st-input-string js-cite-input" type="text" />'
        ].join("\n"));

        return template(this);
    },

    loadData: function(data){
        if (this.options.convertFromMarkdown && data.format !== "html") {
            this.setTextBlockHTML(stToHTML(data.text, this.type));
        } else {
            this.setTextBlockHTML(data.text);
        }

        this.$('.js-cite-input').val(data.cite);
    },

    toMarkdown: function(markdown) {
        return markdown.replace(/^(.+)$/mg,"> $1");
    }
});
