SirTrevor.Blocks.Song = (function () {

    return SirTrevor.Block.extend({

        provider: {
            regex: /id=(.+)/,
            html: "<iframe src=\"http://gakku.kz/ru/music/embed/{{remote_id}}\" width=\"500\" height=\"100\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\" style=\"max-width: 100%;\" allowfullscreen> </iframe>"
        },

        type: "song",
        title: function () {
            return 'Песня';
        },

        pastable: true,

        paste_options: {
            html: "<div style=\"text-align:center; padding:20px;\">Вставьте ссылку на песню<br /><input type=\"text\" class=\"st-paste-block\" style=\"width: 100%\" placeholder=\"Ссылка на песню\"></div>"
        },

        icon_name: '<i class="st-icon-f st-music"></i>',

        loadData: function (data) {

            //this.$editor.addClass("");

            var embed_string = this.provider.html
                .replace("{{remote_id}}", data.remote_id);

            this.$editor.html(embed_string);
        },

        onContentPasted: function (event) {
            this.handleDropPaste($(event.target).val());
        },

        handleDropPaste: function (url) {
            var match, data;

            match = this.provider.regex.exec(url);

            if (match !== null && !_.isUndefined(match[1])) {
                data = {
                    remote_id: match[1]
                };

                this.setAndLoadData(data);
            }
        },

        onDrop: function (transferData) {
            var url = transferData.getData("text/plain");
            this.handleDropPaste(url);
        }
    });

})();