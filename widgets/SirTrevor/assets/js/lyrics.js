SirTrevor.Blocks.Lyrics = SirTrevor.Block.extend({

    type: "lyrics",

    title: function() { return 'Текст песни'; },

    editorHTML: '<input type=text class="st-input-string js-caption-input" name="caption" placeholder="Название песни" style="width: 100%; margin-top: 10px; text-align: center">' +
                '<input type=text class="st-input-string js-words-input" name="words" placeholder="Слова" style="width: 100%; margin-top: 10px; text-align: center">' +
                '<input type=text class="st-input-string js-music-input" name="music" placeholder="Музыка" style="width: 100%; margin-top: 10px; text-align: center">' +
                '<div class="st-required st-text-block" contenteditable="true" style="text-align: left; width: 100%; font-size: 0.90em; padding: 4px;" placeholder="Текст песни"></div>',

    icon_name: '<i class="st-icon-f st-file-text2"></i>',

    loadData: function(data){
        this.getTextBlock().html(SirTrevor.toHTML(data.text, this.type));
        this.$('.js-caption-input').val(data.caption);
        this.$('.js-words-input').val(data.words);
        this.$('.js-music-input').val(data.music);
    }
});