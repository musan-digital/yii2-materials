SirTrevor.Blocks.Gallery = SirTrevor.Block.extend({

    droppable: true,
    uploadable: true,

    type: "gallery",
    title: function () {
        return 'Gallery'
    },
    icon_name: '<i class="st-icon-f st-images"></i>',


    proxied: ['handleFileUpload'],


    loadData: function(data){
        this.addFilesList();
        this.$editor.show();
        if (data && data.length > 0) {
            _.each(data, this.addFileToList, this);
            this.$inputs.show();
        }
    },

    addFilesList: function() {
        if (!_.isUndefined(this.$files)) { return; }

        var $files = $("<div>", { class: "st-files-list" });
        this.$editor.append($files);
        this.$files = $files;
    },

    onBlockRender: function(){
        /* Setup the upload button */
        this.$inputs.find('button').bind('click', function(ev){ ev.preventDefault(); });
        this.$inputs.find('input').on('change', _.bind(function(ev){
            this.onDrop(ev.currentTarget);
        }, this));

        this.addFilesList();
    },

    onDrop: function(transferData) {
        if (transferData.files.length === 0) { return; }
        this.$editor.show();
        _.each(transferData.files, this.handleFileUpload, this);
    },

    handleFileUpload: function(file) {
        SirTrevor.EventBus.trigger('setSubmitButton', ['Please wait...']);
        // Add the file to the list
        this.addFileToList(file);
        this.uploader(file, this.onUploadSuccess, this.onUploadFail);
    },

    onUploadSuccess: function(data, file) {
        data.name = file.name;

        var fileUrl = data.url,
            $link = $("<a>", { html: file.name, href: fileUrl });

        this.$files.find('[data-name="'+file.name+'"]').html($link);
        this.ready();

        // Add or extend the current item
        var currentData = this.getData();

        currentData.files = currentData.files || [];
        currentData.files.push(data);

        this.setData(currentData.files);
    },

    onUploadFail: function(file) {
        this.addMessage("Problem. Could not upload the images.");
        this.removeFileFromList(file);
        this.ready();
    },

    addFileToList: function(file) {

        urlAPI = (typeof URL !== "undefined") ? URL : (typeof webkitURL !== "undefined") ? webkitURL : null;

        var $inner = file.name;

        if (!_.isUndefined(file.url)) {
            $inner = $("<a>", { html: file.name, href: file.url });
        }

        if (file.hash == undefined){
            var link = urlAPI.createObjectURL(file);
        }
        else {
            var link = file.filelink;
        }

        var $file = $('<img>', { src: link, height: 'auto', width: 150 });
        /*
         var $file = $("<li>", {
         class: 'st-uploaded-file',
         html: $inner,
         "data-name": file.name
         });

         */
        this.$files.append($file);
    },

    removeFileFromList: function(file) {
        this.$files.find('[data-name="'+file.name+'"]').remove();
    }


});
