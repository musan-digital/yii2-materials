<?php

namespace lafacoder\modules\materials;
use Symfony\Component\Debug\Exception\ClassNotFoundException;

/**
 * materials module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'lafacoder\modules\materials\controllers';

    public $types;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function getTypeClass($type)
    {
        if (isset($this->types[$type]))
        {
            return $this->types[$type]['class'];
        }
        else {
            throw new ClassNotFoundException;
        }
    }
}
