<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m161213_124230_create_table_blocks extends Migration
{
    public function up()
    {
        $this->renameColumn('materials', 'type_id', 'type');
        $this->alterColumn('materials', 'type', $this->string());
    }

    public function down()
    {
        $this->renameColumn('materials', 'type', 'type_id');
        $this->alterColumn('materials', 'type_id', $this->integer());
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
