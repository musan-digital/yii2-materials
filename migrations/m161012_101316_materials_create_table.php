<?php

use yii\db\Migration;

class m161012_101316_materials_create_table extends Migration
{
    public function up()
    {
        $this->createTable('materials', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'type_id' => $this->integer(),
            'title' => $this->string(255),
            'subtitle' => $this->string(1024),
            'slug' => $this->string(255),
            'image' => $this->string(255),
            'create_date' => $this->dateTime(),
            'update_date' => $this->dateTime(),
            'publish_date' => $this->dateTime(),
            'is_active' => $this->boolean()->defaultValue(0),
            'is_delete' => $this->boolean()->defaultValue(0),
            'views' => $this->integer()->defaultValue(0),
        ]);
    }

    public function down()
    {
        $this->dropTable('materials');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
