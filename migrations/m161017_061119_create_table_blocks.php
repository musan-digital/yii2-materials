<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m161017_061119_create_table_blocks extends Migration
{
    public function up()
    {
        $this->createTable('blocks', [
            'id' => $this->primaryKey(),
            'material_id' => Schema::TYPE_INTEGER,
            'value' => Schema::TYPE_TEXT,
            'code' => Schema::TYPE_TEXT,
            'type' => Schema::TYPE_STRING,
            'order' => Schema::TYPE_INTEGER,
            'lang' => Schema::TYPE_STRING,
            'is_deleted' => Schema::TYPE_BOOLEAN
        ]);
    }

    public function down()
    {
        $this->dropTable('blocks');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
