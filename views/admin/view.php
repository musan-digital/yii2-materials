<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use mitrii\attachments\helpers\Image;
use lafacoder\modules\materials\models\Material;

/* @var $this yii\web\View */
/* @var $model lafacoder\modules\materials\models\Material */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Material::getType($type)['label'], 'url' => ['index', 'type' => $type]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'title',
            'subtitle',
            [
                'label' => 'Фото',
                'value' => Image::url($model->image, 100, 100),
                'format' => ['image', []],
            ],
            'create_date',
            'update_date',
            'publish_date',
            'is_active:boolean',
            'views',
            'slug',
        ],
    ]) ?>

</div>
