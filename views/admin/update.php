<?php

use lafacoder\modules\materials\models\Material;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model lafacoder\modules\materials\models\Material */

$this->title = 'Изменить:' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Material::getType($type)['label'], 'url' => ['index', 'type' => $type]];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="material-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'type' => $type
    ]) ?>

</div>
