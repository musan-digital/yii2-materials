<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use kartik\switchinput\SwitchInput;
use lafacoder\modules\materials\models\Material;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model lafacoder\modules\materials\models\Material */
/* @var $form yii\widgets\ActiveForm */
$model->types();
?>

<div class="material-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'is_active')->widget(SwitchInput::classname(), [
                'type' => SwitchInput::CHECKBOX,
                'inlineLabel' => false,
                'pluginOptions' => [
                    'onText' => 'Да',
                    'offText' => 'Нет',
                ]
            ]) ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'subtitle')->textarea(['raw' => 5]) ?>

            <?= $form->field($model, 'image')->widget(\mitrii\attachments\widgets\PhotoWidget::className(), [])->label('Главная картинка новости мин. размер: 620 x 340') ?>

            <?= $form->field($model, 'publish_date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Дата публикации ...'],
                'language' => 'ru',
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd hh:ii:ss'
                ]
            ]); ?>

            <?= $form->field($model, 'raw_blocks_ru')->label(false)->widget(\lafacoder\modules\materials\widgets\SirTrevor\SirTrevor::className(), [
                'uploadUrl' => \yii\helpers\Url::to(['/attachment/upload/add', 'name'=>'attachment[file]']),
                'blockTypes' => Material::getType($model->type)['blockTypes'],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?php if (isset($model->getType($type)['properties'])): ?>
                <?php foreach ($model->getType($type)['properties'] as $property): ?>
                    <?= $this->render('@backend/propertiesViews/'.$property, ['form' => $form, 'model' => $model]) ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
