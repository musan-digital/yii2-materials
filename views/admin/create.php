<?php

use lafacoder\modules\materials\models\Material;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model lafacoder\modules\materials\models\Material */

$this->title = 'Добавить '.Material::getType($type)['label'];
$this->params['breadcrumbs'][] = ['label' => Material::getType($type)['label'], 'url' => ['index', 'type' => $type]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'type' => $type
    ]) ?>

</div>
