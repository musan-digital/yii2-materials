<?php

use yii\helpers\Html;
use yii\grid\GridView;
use mitrii\attachments\helpers\Image;
use lafacoder\modules\materials\models\Material;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\materials\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Material::getType($type)['label'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить ' . Material::getType($type)['label'], ['create', 'type' => $type], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Фото',
                'format' => 'image',
                'value' => function ($data) {
                    return Image::url($data->image, 80, 60);
                },
            ],
            'title',
            'publish_date',

            [
                'value'=> function($data) { return Html::a(Html::tag('span','',['class'=>'glyphicon glyphicon-eye-open']), \yii\helpers\Url::to(['/materials/admin/view', 'id' => $data->id])); },
                'format' => 'raw'
            ],

            [
                'value'=> function($data) { return Html::a(Html::tag('span','',['class'=>'glyphicon glyphicon-pencil']), \yii\helpers\Url::to(['/materials/admin/update', 'id' => $data->id])); },
                'format' => 'raw'
            ],

            [
                'value'=> function($data) { return Html::a(Html::tag('span','',['class'=>'glyphicon glyphicon-trash']), \yii\helpers\Url::to(['/materials/admin/delete', 'id' => $data->id])); },
                'format' => 'raw'
            ],
        ],
    ]); ?>
</div>
